
/**
 *
 */

module.exports = function(app){
  var userController = require('./controllers/user') (app)
   , rootController = require('./controllers/root') (app)
   , dabbawalaController = require('./controllers/tiffinSupplier') (app)
   , cartController = require('./controllers/cart') (app)
   , orderController = require('./controllers/order') (app)
   , calendarController = require('./controllers/calendar') (app)
   , passportConfig = require('./config/passport-config.js');   

  app.get('/', rootController.landing);
  app.get('/users/user', rootController.user);
  app.get('/admin'
    , passportConfig.isAuthenticated
    , passportConfig.ensureAdmin
    , rootController.adminDashboard);


  app.get('/users/confirm', userController.confirmEmail);
  app.get('/users/resetPassword', userController.renderResetPasswordPage);
  app.get('/users/resetPasswordPage', rootController.resetPassword);
  
  app.get('/users/profile', passportConfig.isAuthenticated, rootController.userProfile);
  app.get('/users/homepage', passportConfig.isAuthenticated, rootController.homepage);
  
  app.post('/users/authenticate', userController.authenticate);
  app.get('/users/fbauth', userController.startFbAuthentication);
  app.get('/users/fbAuthenticationComplete', userController.onFbAuthenticationComplete);
  app.post('/users/forgotPassword', userController.forgotPassword);
  app.post('/users/resetPassword', userController.resetPassword)
  app.get('/admin/logout', userController.logout);

  app.get('/users/loggedIn',userController.userLoggedIn);
  app.post('/users'
    ,userController.create);
  app.get('/users/:id'
    ,userController.search);
  app.put('/users/:id'
    , passportConfig.isAuthenticated
    ,userController.update);
  app.delete('/users/:id'
    , passportConfig.isAuthenticated
    ,userController.delete);
  app.put('/changePassword/:id',userController.changePassword);

  app.post('/tiffinBoxSupplier'
    , passportConfig.isAuthenticated
    , passportConfig.ensureAdmin
    , dabbawalaController.create);
  app.put('/tiffinBoxSupplier/:id'
    , passportConfig.isAuthenticated
    , passportConfig.ensureAdmin
    , dabbawalaController.update);


  app.get('/tiffinBoxSupplier/search',dabbawalaController.search);
  app.get('/tiffinBoxSupplier/checkout',dabbawalaController.checkout);
  app.get('/tiffinBoxSupplier/filter',dabbawalaController.filter)
  app.get('/tiffinBoxSupplier', dabbawalaController.index);
  app.get('/tiffinBoxSupplier/:id', dabbawalaController.show);

  app.delete('/tiffinBoxSupplier/:id'
    , passportConfig.isAuthenticated
    , passportConfig.ensureAdmin
    , dabbawalaController.delete);
  
  app.post('/tiffinBoxSupplierMenu'
    , passportConfig.isAuthenticated
    , passportConfig.ensureAdmin
    , dabbawalaController.addMenu);

  app.put('/tiffinBoxSupplierMenu/:dabbawalaId/:menuId'
    , passportConfig.isAuthenticated
    , passportConfig.ensureAdmin
    , dabbawalaController.updateMenu);

  app.post('/tiffinBoxSupplierMenu/:dabbawalaId/:menuId'
    , passportConfig.isAuthenticated
    , passportConfig.ensureAdmin
    , dabbawalaController.deleteMenu);

  app.post('/calendar/menuDate/:dabbawalaId'
     ,calendarController.assignMenuDate);
  app.get('/calendar/menuDate'
     ,calendarController.getMenuDate);

  app.post('/cart/addtocart',
    cartController.addToCart);
  app.get('/cart/addtocart/:id',
    cartController.getToCart);
   app.put('/cart/addtocart/:id/:singlecartid',
    cartController.deleteToCart);
  app.put('/cart/addtocart/:id',
    cartController.updateToCart);

  app.post('/order',
    orderController.addToOrder);
  app.get('/order/userOrders',
    orderController.getUserOrders);
  app.get('/order/getOrderDetails',
    orderController.getAllOrders);
  app.get('/order/getSupplierOrderDetails/:id',
    orderController.getSupplierOrders);

  app.post('/tiffinBoxSupplierRating/:dabbawalaId',
    dabbawalaController.addRating);
  app.post('/tiffinBoxSupplierMenuRating/:dabbawalaId/:menuId',
    dabbawalaController.addMenuRating);
 
};
