var mongoose = require('mongoose') 
  , TiffinSupplier = require('../models/TiffinboxSuppliers')
  , Order = require('../models/Order')
  , config = require('../config/config')
  , Cart = require('../models/Cart')
  , User = require('../models/User');


module.exports = function(app) {
  
  var order = {};

  order.addToOrder = function(req, res, next){
    //console.log('in addToOrder api');
    var objOrder= req.body.order;

    var order = new Order(objOrder);
    
    order.save(function(err,orders){
      if(err){return next(err);};
      if(orders){
        Cart.findByIdAndRemove(orders._id,function(err,cart){
          if (err){return next(err);}
          //console.log('---------###--------Delet cart and its id:'+cart._id);
          req.session.cart = undefined;
        });

        User.findById(orders.userId, function(err, user){
          if (err) return next(err);
          var messageData= 'Your Order details are:\nOrderId-: '+orders.id+''; 

          var ordersDetailsArray = [];
          for(var i = 0; i < orders.orderDetails.length; i++){
            ordersDetailsArray.push(orders.orderDetails[i].tiffinboxSupplier);

            TiffinSupplier.update({"menu._id" : orders.orderDetails[i].menuId}
              , {$addToSet : {"menu.$.orderedUser": orders.userId}}
              , function(err,total,doc){
                  if(err){ return next(err);};
                  console.log('updated menu:');
                  console.log(doc);
              });
          }


          TiffinSupplier.find({_id:{$in:ordersDetailsArray}},function(err, ts){
            if(err) return next(err);
            // console.log(ts);
            for(var j=0; j<ts.length; j++){
              var msg = 'order from '+user.email;
              var TSemail = {
                to: ts[j].email,
                message: msg,
                subject: config.email.subject.orderConfirmedEmail
              };

              messageData+= '\nTiffinSupplier: '+ts[j].name; 
              app.monq.sendEmail(TSemail, function(err){
                if(err) { return next(err);};
              }); 
              // add user id into each tiffin Supplier
              TiffinSupplier.findByIdAndUpdate(ts[j]._id, {$addToSet: { orderedUser: orders.userId }}, function (err, doc) {
                if (err) return handleError(err);
                if(doc)
                console.log('userId addeed in TS collection!'+orders.userId);
              else
                console.log('not update'+orders.userId);
              });
            }  


            var userEmail = {
              to: user.email,
              message: messageData,
              subject: config.email.subject.orderConfirmedEmail
            };
            app.monq.sendEmail(userEmail, function(err){
              if(err) { return next(err);};
            }); 
            return res.json(orders);  
          });

        });

        // // Twilio Credentials 
        // var accountSid = 'AC477ef281834b600ea2cb0abeafbe4ac5'; 
        // var authToken = '2dad05a637aecd16976f63108d92b8b2'; 
         
        // //require the Twilio module and create a REST client 
        // var client = require('twilio')(accountSid, authToken); 
         
        // client.messages.create({ 
        //   to: "9673268248", 
        //   from: "+18622596207", 
        //   body: "Hi, salim this is test sms", 
        //   mediaUrl: "PuneDabbawala.in",  
        // }, function(err, message) { 
        //   if (err){
        //     console.log('error:');
        //     console.log(err);
        //   }else{
        //    console.log('success:');          
        //    console.log(message); 
        // });
      }
    });  
  };
  order.getAllOrders = function(req,res,next){
    Order.find().exec(function(err,orders){
      if(err){
        return next(err);
      }
      return res.json(orders);
    });         
  };
  order.getUserOrders = function(req, res, next){
    //console.log('in getUserOrders api...!');
    var today = new Date()
    today.setDate(today.getDate()-7)

    Order.find({userId: req.query.userId, orderDate:{$gte:today}})
      .populate('orderDetails.tiffinboxSupplier')
      .exec(function(err,orders){
        if(err){
          return next(err);
        }
        return res.json(orders);
      });    
  };
  order.getSupplierOrders = function(req,res,next){
    var tiffinboxSupplierId = req.params.id;
      Order.find({ orderDetails : { $elemMatch : { tiffinboxSupplier: tiffinboxSupplierId}}}).exec(function(err,orders){
      if(err){ return next(err);}
        return res.json(orders);
    });
  }

  return order;
};