var mongoose = require('mongoose')
  , passport = require('passport')
  , util = require('../utils')
  , config = require('../config/config')
  , TiffinSupplier = require('../models/TiffinboxSuppliers')
  , Cart = require('../models/Cart')
  , TiffinCalendar = require('../models/TiffinCalendar')
  , User = require('../models/User');


module.exports = function(app) {

  var cart = {};

  cart.addToCart = function(req, res, next){
    //console.log('in addToCart api');    
    var cart = new Cart(req.body);

    if(typeof req.session.cart != 'undefined'){
      var cart1 = req.session.cart; 
      for (var i =0; i<cart1.orderDetails.length; i++){        
        cart.orderDetails.push(cart1.orderDetails[i]);        
      }
    }
    //cart.orderDetails = req.body;
    cart.save(function(err,cart){
      if(err){return next(err);};

      if(cart){
        req.session.cart = null;
        req.session.cart=cart;
        return res.json(cart);              
      }
    });  
	
  };

  cart.getToCart = function(req, res, next){
    //console.log('in getToCart api'+req.params.id);
    Cart.findById(req.params.id)
      .populate('orderDetails.tiffinboxSupplier')
      .exec(function(err, cart) {
        if(err) { return next(err); };

        if(cart) {
          return res.json(cart);
        } else {
          return res.json(404, {error: 'Cart not found!'});
        }
    });
  };

  cart.deleteToCart = function(req, res, next){
    //console.log('in deleteToCart api'+req.params.id);
    Cart.findById(req.params.id,function(err,cart){
      if(err){return next(err);}

      if (cart){
        cart.orderDetails.pull({_id:req.params.singlecartid});

        cart.save(function(err,cart){
          if(err){return next(err);}
          if(cart){
            //pull
            if(cart.orderDetails.length==0){
              console.log('in delete to cart empty cart:'+cart._id);
              req.session.cart = undefined;
            }
            else{
              //push
              req.session.cart = null;
              req.session.cart= cart;              
            }
            return res.json(cart);
            
          }
        })
      }
      else {
        return res.json(404, {error: 'page not found!'});
      }
    });    
  };
  
  cart.updateToCart = function(req, res, next){
    //console.log('in updateToCart api'+req.params.id);
    Cart.findById(req.params.id, function(err,cart){
      if(err){return next(err);}

      if(cart){
        var updateDetails = req.body.updateDetails;
        for (var i = 0; i< cart.orderDetails.length; i++){
          for(var j= 0; j<updateDetails.length; j++){
            if(updateDetails[j].dayId==cart.orderDetails[i]._id){
              var data = updateDetails[j].deliveryAddress;
              // console.log(data);
              cart.orderDetails[i].deliveryAddress=data;
            }
          }
        }
        cart.contactNumber=req.body.contact;
        cart.userId=req.body.userId;
        cart.save(function(err, cart){
          if (err) {return next(err);};

          if(cart){
            return res.json(cart);
          }else{
            return res.json(404, {error: "Cart not found!"})
          }
        });
      }
    });

  };
  return cart;
};