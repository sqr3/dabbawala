var mongoose = require('mongoose')
  , passport = require('passport')
  , util = require('../utils')
  , config = require('../config/config')
  , TiffinSupplier = require('../models/TiffinboxSuppliers')
  , TiffinCalendar = require('../models/TiffinCalendar')
  , User = require('../models/User');


module.exports = function(app) {
  
  var calendar = {};

  calendar.getMenuDate = function (req, res,next) {
  	//console.log('in getMenuDate api');
    var query= req.query.menuDate;
    var arr = query.split(',');
    TiffinCalendar.find({tiffinboxSupplier: {$in: arr}}) 
    .populate('tiffinboxSupplier')
    .exec(function(err,tiffinBoxSuppliers){
      if(err) { return next(err);
      };
      if(tiffinBoxSuppliers){
        res.json(tiffinBoxSuppliers);
      }
      else{
        res.json(404, {error:"TiffinCalendar Not found"});
      }
    });
  }; 


  calendar.assignMenuDate = function(req, res, next){
    //console.log('in assignMenuDate api');
    TiffinCalendar.findOne({tiffinboxSupplier:req.params.dabbawalaId}, function(err,result){
      if (err) { return next(err);};
      if(result){
        result.tiffinboxSupplier = req.params.dabbawalaId;
        result.days.push(req.body);
        result.save(function(err,menuDate){
          if (err) {return next(err);};
          if (menuDate) {
            res.json(menuDate);
          }else{
            return res.json(404, {error: 'page is not found!'})
          }
        });
      }
      else{
        var menuDate = new TiffinCalendar();
        menuDate.tiffinboxSupplier= req.params.dabbawalaId;
        menuDate.days.push(req.body);
        menuDate.save(function(err,menuDate){
          if (err) {return next(err);};
          
          if (menuDate) {
            res.json(menuDate);
          }else{
            return res.json(404, {error: 'page is not found!'})
          }
        });
      }
    });
  };

  return calendar;
};