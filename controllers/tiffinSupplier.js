/**
 *
 */

var mongoose = require('mongoose')
  , passport = require('passport')
  , url = require('url')
  , util = require('../utils')
  , config = require('../config/config')
  , User = require('../models/User')
  , Order = require('../models/Order')
  , TiffinCalendar = require('../models/TiffinCalendar')
  , TiffinboxSupplier = require('../models/TiffinboxSuppliers');


module.exports = function(app) {
  
  var tiffinboxSupplier = {};

  tiffinboxSupplier.create = function(req, res, next){
    var tiffinboxSupplier = new TiffinboxSupplier(req.body);

    tiffinboxSupplier.save(function(err, tiffinboxSupplier){
      if (err) { return next(err)};
      if(tiffinboxSupplier) {
        return res.json(tiffinboxSupplier);
      }
      else {
        return 
        res.status(500).json({error: 'Unable to add TiffinboxSupplier!'});
      }     
    });
  };

  tiffinboxSupplier.index = function(req, res, next){
    TiffinboxSupplier.find({},function(err,dabbawalaList){
      if (err) {return next(err);};
      if (dabbawalaList){
        res.json(dabbawalaList);
      }else{
        return res.json(404, {error: 'TiffinboxSuppliers not found !'})
      }
    });
  };

  tiffinboxSupplier.show = function(req,res){
    //console.log('in show api'+req.params.id);

    TiffinboxSupplier.findById(req.params.id)
      .populate('team reviews.user menu.reviews.user')
      // .populate({
      //   path: 'reviews.user',
      //   select: 'name -_id'
      // })
      .exec(function(err, tiffinBoxSupplier) {
        if(err) { return next(err); };

        if(tiffinBoxSupplier) {
          return res.json(tiffinBoxSupplier);
        } else {
          return res.json(404, {error: 'Tiffin box supplier not found!'});
        }
    });
  };

  tiffinboxSupplier.addMenu = function(req, res, next){
    //console.log('in Menu api'+req.query.dabbawalaId);
    TiffinboxSupplier.findById(
      req.query.dabbawalaId,
      function(err,tiffinboxSupplier){
        if(err){return next(err)}
        if(tiffinboxSupplier){
          tiffinboxSupplier.menu.push(req.body);

          tiffinboxSupplier.save(function(err,tiffinboxSupplier){
            if(err){
              return next(err);
            }
            else{
              res.json(tiffinboxSupplier);
            }
          });
        }  
    });
  };

  tiffinboxSupplier.search = function (req, res, next) {

    var regex = new RegExp(req.query.query, 'i');
    //console.log('in search'+req.query.query);
    var query = {$or: [
      {name: { $regex: regex}}
      ,{distributionAreas: {$in: [regex]}}
      ,{category: {$in: [regex]}}
      ,{mealType: {$in: [regex]}}
      ,{orderType: {$in: [regex]}}
      ]};

    TiffinboxSupplier.find(query, function(err, tiffinBoxSuppliers) {
      if(err) { return next(err); };

      res.json(tiffinBoxSuppliers);
    });
  };

  tiffinboxSupplier.checkout = function (req, res, next) {
    //console.log('in checkout api');
    var query= req.query.query;
    var arr = query.split(',');
    var myarr= JSON.parse(arr);

    var ts=[];
    TiffinboxSupplier.find({_id: {$in: myarr}}, function(err,tiffinBoxSuppliers){
      if(err) { return next(err); };

      res.json(tiffinBoxSuppliers);
    });  
  };


  tiffinboxSupplier.filter = function(req,res,next){
  
    var filterValue=req.query.query;
    var filterval=JSON.parse(filterValue);
    var regex = new RegExp(req.query.search, 'i');

    var cat= filterval.category;
    var meal=filterval.mealType;
    var order=filterval.orderType;

    var que=[];
    if( typeof cat=='object'){
      que.push({category:{ $all :cat}});
    }
    else if ( typeof cat=='string'){
      que.push({category:{$all: cat}});
      }
    if(typeof meal=='object'){   
      que.push({mealType:{ $all :meal}});
    }
    else if(typeof meal=='string'){
      que.push({mealType:{$all: meal}});
    }
    if(typeof order=='object'){ 
      que.push({orderType:{ $all :order}});
    }
    else if(typeof order=='string'){   
     que.push({orderType:{$all: order}});
    }
    var query={$and:
                  [{$or: 
                    [{name: { $regex: regex}}
                    ,{distributionAreas: {$in: [regex]}}
                    ,{category: {$in: [regex]}}
                    ,{mealType: {$in: [regex]}}
                    ,{orderType: {$in: [regex]}}]
                  }, {$and: que}] 
                };
    TiffinboxSupplier.find(query, function(err, tiffinboxSupplier) {
      if(err) { return next(err); };

      res.json(tiffinboxSupplier);
    });
  };
 
  tiffinboxSupplier.delete = function (req, res, next) {
    if(req.params.id){
      TiffinboxSupplier.findById(
        req.params.id,
        function(err,tiffinBoxSupplier){
          if(!err){
            tiffinBoxSupplier.remove();
            res.json(tiffinBoxSupplier);
          }
      });
    }  
  };

  tiffinboxSupplier.update = function (req, res, next) {
    //console.log("in update api"+req.params.id);
    if(req.params.id){
      TiffinboxSupplier.findByIdAndUpdate(req.params.id, req.body,
        function(err,tiffinBoxSupplier){
          if(!err){
            return res.json(tiffinBoxSupplier);
          } else {
            return next(err);
          }
        });
    }  
  };

  tiffinboxSupplier.updateMenu= function(req,res, next){
    //console.log('in updateMenu api');
    TiffinboxSupplier.findById(
      req.params.dabbawalaId,
      function(err,tiffinboxSupplier){
        if(err){return next(err)}
        if(tiffinboxSupplier){
          var menuId = req.params.menuId;
          for( var i= 0; i < tiffinboxSupplier.menu.length; i++){
            if(tiffinboxSupplier.menu[i].id === menuId){
              //console.log('matched');
              tiffinboxSupplier.menu[i].id= menuId;
              tiffinboxSupplier.menu[i].name=req.body.name;
              tiffinboxSupplier.menu[i].ingredients =req.body.ingredients;
              tiffinboxSupplier.menu[i].description=req.body.description;
              tiffinboxSupplier.menu[i].category=req.body.category;
              tiffinboxSupplier.menu[i].mealType=req.body.mealType;
              tiffinboxSupplier.menu[i].fullPrice=req.body.fullPrice;
              tiffinboxSupplier.menu[i].discountedPrice=req.body.discountedPrice;
              break;
            }
          }

          tiffinboxSupplier.save(function(err,tiffinboxSupplier){
            if(err){
              return next(err);
            }
            else{
              res.json(tiffinboxSupplier);
            }
          });
        }  
    });
  };
  
  tiffinboxSupplier.deleteMenu = function(req, res, next){
    //console.log('in deleteMenu api');
    TiffinboxSupplier.findById(
      req.params.dabbawalaId,
      function(err,tiffinboxSupplier){
        if(err){return next(err)}
        if(tiffinboxSupplier){
          var menuId = req.params.menuId;
          tiffinboxSupplier.menu.pull({_id: menuId});

          tiffinboxSupplier.save(function(err,tiffinboxSupplier){
            if(err){
              return next(err);
            }
            else{
              res.json(tiffinboxSupplier);
            }
          });
        }  
    });
  };

  tiffinboxSupplier.addRating = function(req, res, next){
    console.log('addRating api called...!');
    // Order.find({userId:req.body.user}, function(err, orders){
    //   if(err){ return next(err);}
    //   if(orders){
    //     //if()
    //     var flag= false;
    //     console.log('user order found');
    //     for(var i = 0; i< orders.length; i++){
    //       var order = orders[i];
    //       for(var j=0; j< order.orderDetails.length; j++){
    //         if (order.orderDetails[j].tiffinboxSupplier== req.body.dabbawalaId){
    //           console.log('found'+order.orderDetails[j].tiffinboxSupplier);
    //           flag= true;  
    //           break;                      
    //         }
    //       }
    //     }
        
    //     if(flag){
    //       TiffinboxSupplier.findById(
    //         req.params.dabbawalaId,
    //         function(err,tiffinBoxSupplier){
    //           if(err) { return next(err);}

    //           if(tiffinBoxSupplier){
    //             console.log('record found!');
    //             var flag2 = false;
    //             for(var i=0; i<tiffinBoxSupplier.reviews.length; i++){
    //               console.log('i:'+i)
    //               if(tiffinBoxSupplier.reviews[i].user == req.body.user){
    //                 console.log('record matched');
    //                 tiffinBoxSupplier.reviews[i].user = req.body.user;
    //                 tiffinBoxSupplier.reviews[i].rate = req.body.rate;
    //                if(req.body.review){   
    //                console.log('in user enters reviews..............');                     
    //                  tiffinBoxSupplier.reviews[i].review = req.body.review;
    //                  tiffinBoxSupplier.reviews[i].title = req.body.title;
    //                }
    //                 flag2 = true;
    //                 break;
    //               }
    //             }

    //             if(!flag2){
    //               console.log('user not exist!');
    //               tiffinBoxSupplier.reviews.push(req.body);
    //             }

    //             tiffinBoxSupplier.save(function(err, ts){
    //               if(err) { return next(err);}
    //               console.log(ts.reviews);
    //               return res.json(ts);


    //             });
    //           }else{
    //             return res.json(404, {error: 'Tiffin Supplier not Found!'});
    //           }
    //         }); 
    //     }else{
    //       console.log('order not found for specified tiffin supplier');
    //       res.json(404, {error:'order not found for specified tiffin supplier'});
    //     }

    //   }else{
    //     console.log('user order not found!');
    //     res.json(404, {error:'users order not found'});
    //   }
    // });

    TiffinboxSupplier.findById(
      req.params.dabbawalaId,
      function(err,tiffinBoxSupplier){
        if(err) { return next(err);}

        if(tiffinBoxSupplier){
          console.log('record found!');
          var flag2 = false;
          for(var i=0; i<tiffinBoxSupplier.reviews.length; i++){
            console.log('i:'+i)
            if(tiffinBoxSupplier.reviews[i].user == req.body.user){
              console.log('record matched');
              tiffinBoxSupplier.reviews[i].user = req.body.user;
              tiffinBoxSupplier.reviews[i].rate = req.body.rate;
              tiffinBoxSupplier.reviews[i].reviewAt = new Date();
              if(req.body.review){   
                console.log('in user enters reviews..............');                     
                tiffinBoxSupplier.reviews[i].review = req.body.review;
                tiffinBoxSupplier.reviews[i].title = req.body.title;                
              }
              flag2 = true;
              break;
            }
          }
          if(!flag2){
            console.log('user not exist!');
            tiffinBoxSupplier.reviews.push(req.body);
            tiffinBoxSupplier.reviews.reviewAt = new Date();
          }

          tiffinBoxSupplier.save(function(err, ts){
            if(err) { return next(err);}
            console.log(ts.reviews);
            return res.json(ts);
          });
        }else{
          return res.json(404, {error: 'Tiffin Supplier not Found!'});
        }
    }); 
  };

  tiffinboxSupplier.addMenuRating = function(req, res, next){
    console.log('addMenuRating api called...!');
    // Order.find({userId:req.body.user}, function(err, orders){
    //   if(err){ return next(err);}
    //   if(orders){
    //     //if()
    //     var flag= false;
    //     console.log('user order found');
    //     console.log(order);
    //     for(var i = 0; i< orders.length; i++){
    //       var order = orders[i];
    //       for(var j=0; j< order.orderDetails.length; j++){
    //         if (order.orderDetails[j].menuId== req.body.menuId){
    //           console.log('found'+order.orderDetails[j].menuId);
    //           flag= true;                        
    //         }
    //       }
    //     }
                   
    //     if(flag){
    //       TiffinboxSupplier.findById(
    //         req.params.dabbawalaId,
    //         function(err,tiffinBoxSupplier){
    //           if(err) { return next(err);}

    //           if(tiffinBoxSupplier){
    //             var data= tiffinBoxSupplier.menu;
    //             for(var i = 0; i<data.length; i++){
    //               if(req.params.menuId== data[i]._id){
    //                 console.log('menu matched')
    //                 var flag = false;
    //                 var menuData = data[i].reviews;
    //                 for(var j=0; j<menuData.length; j++){
    //                   if (menuData[j].user == req.body.user){
    //                     console.log('record matched(user exist)!:'+menuData[j].user);
    //                     menuData[j].user = req.body.user;
    //                     menuData[j].rate = req.body.rate;
    //                    if(req.body.review){
    //                      menuData[j].review = req.body.review;
    //                      menuData[j].title = req.body.title;
    //                    }
                       
                        
    //                     flag = true;
    //                     break;
    //                   }
    //                 } // inner for

    //                 if(!flag){
    //                   console.log('user does not exist!');
    //                   menuData.push(req.body);
    //                 }

    //                 tiffinBoxSupplier.save(function(err, ts){
    //                   if(err) { return next(err);}
    //                   //console.log(ts);
    //                   return res.json(ts);
    //                 });

    //                 break;
    //               } //if
    //             } //outer for
    //           }else{
    //             return res.json(404, {error: 'Tiffin Supplier not Found!'});
    //           }
    //         });        
    //     }else{
    //       console.log('order not found for specified Menu!');
    //       res.json(404, {error:'order not found for specified menu!'});
    //     }

    //   }else{
    //     console.log('user order not found!');
    //     res.json(404, {error:'users order not found'});
    //   }
    // });

    TiffinboxSupplier.findById(
      req.params.dabbawalaId,
      function(err,tiffinBoxSupplier){
        if(err) { return next(err);}

        if(tiffinBoxSupplier){
          var data= tiffinBoxSupplier.menu;
          for(var i = 0; i<data.length; i++){
            if(req.params.menuId== data[i]._id){
              console.log('menu matched')
              var flag = false;
              var menuData = data[i].reviews;
              for(var j=0; j<menuData.length; j++){
                if (menuData[j].user == req.body.user){
                  console.log('record matched(user exist)!:'+menuData[j].user);
                  menuData[j].user = req.body.user;
                  menuData[j].rate = req.body.rate;
                  menuData[j].reviewAt = new Date();
                  if(req.body.review){
                    menuData[j].review = req.body.review;
                    menuData[j].title = req.body.title;
                  }              
                  flag = true;
                  break;
                }
              } // inner for

              if(!flag){
                console.log('user does not exist!');
                menuData.push(req.body);
                menuData.reviewAt = new Date();
              }

              tiffinBoxSupplier.save(function(err, ts){
                if(err) { return next(err);}
                //console.log(ts);
                return res.json(ts);
              });
              if(flag)
                break;
            } //if
          } //outer for
        }else{
          return res.json(404, {error: 'Tiffin Supplier not Found!'});
        }
      });        
  };
 
  return tiffinboxSupplier;
};

