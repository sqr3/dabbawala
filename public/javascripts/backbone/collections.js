
 var app = app || {};

(function () {
	'use strict';

 app.searchTiffinboxSupplier = Backbone.Collection.extend({
    initialize: function(options) {
      this.options = options;
    },
    url:function() {
      return '/tiffinBoxSupplier/search?query=' + this.options.query;
    }

  });

 
 app.GetMenuDate = Backbone.Collection.extend({
    initialize: function(options) {
      this.options = options;
    },
    url:function() {
      return '/calendar/menuDate?menuDate=' + this.options.menuDate;
    }

  });
/////////////////////////////////
app.ProcessOrder = Backbone.Collection.extend({
    initialize: function(options) {
      this.options = options;
    },
    url:function() {
      return '/tiffinBoxSupplier/processOrder?query=' + this.options.query;
    }

  });
///////////////////////////////////
app.CartCollections = Backbone.Collection.extend({
    initialize: function(options) {
      this.options = options;
    },
    url:function() {
      if(this.options.query)
      return '/cart/addtocart?query=' + this.options.query;
    else
      return '/cart';
    }

  });

app.orderCollections = Backbone.Collection.extend({
    initialize: function(options) {
      this.options = options;
    },
    url:function() {
      console.log('in collection.js'+this.options.userId);
      if(this.options.userId)
      return '/order/userOrders?userId=' + this.options.userId;
   
    }

  });

  app.SearchFilterResult = Backbone.Collection.extend({
    initialize: function(options ,search) {
      this.options = options;
      this.search=search;     
    },
    url:function() {
      return '/tiffinBoxSupplier/filter?query=' + this.options+'&search='+this.search;
    }

  });

/* app.SearchFilterResult = Backbone.Collection.extend({
    url: '/tiffinBoxSupplier/filter' 
    
  });
 */
  

})();

















