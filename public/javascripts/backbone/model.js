var app = app || {};

(function () {
	'use strict';
	app.User = Backbone.Model.extend({
    urlRoot: function(){
      if(this.get('dabbawalaId'))
        return '/users/?dabbawalaId=' + this.get('dabbawalaId');
      else{
        console.log('Model');
        return '/users';  
      }
    }    
  });
 
  app.addTiffinBoxSupplier = Backbone.Model.extend({
    url: function() {
      if(this.get('id')) {
        return '/tiffinBoxSupplier/' + this.get('id');    
      } else {
        return '/tiffinBoxSupplier/';  
      };
    },
    parse: function(data){
      var menuArray = data.menu;
      var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      var months = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug","Sep","Oct","Nov", "Dec"];
      // set the menu reviews average and date formate
      for (var i=0; i<menuArray.length; i++){
        var menuReviews= menuArray[i].reviews;
        var total = 0;
        for(var j= 0; j<menuReviews.length; j++){
          total+= menuReviews[j].rate;
          //set reviews date format
          var reviewsDateFormatted = new Date(menuReviews[j].reviewAt);
          reviewsDateFormatted = months[reviewsDateFormatted.getMonth()] +", "+ reviewsDateFormatted.getDate()+ ", "+ reviewsDateFormatted.getFullYear(); 
          menuReviews[j].reviewsDateFormatted = reviewsDateFormatted;
        }
          menuArray[i].avgRating = menuArray[i].totalRating= 0;
          menuArray[i].avgRating = total/menuReviews.length;
          menuArray[i].totalRating = menuReviews.length;
      }
      // set the review date format
      var total = 0;
      for (var i = 0; i<data.reviews.length; i++){   
        total += data.reviews[i].rate;  
        var reviewsDateFormatted = new Date(data.reviews[i].reviewAt);
        reviewsDateFormatted = months[reviewsDateFormatted.getMonth()] +", "+ reviewsDateFormatted.getDate()+ ", "+ reviewsDateFormatted.getFullYear(); 
        data.reviews[i].reviewsDateFormatted = reviewsDateFormatted;
      }
      // console.log(typeof(total));
      // console.log(typeof(data.reviews.length));
      data.avgRating = data.totalRating = 0;
      //console.log(data.avgRating);
      data.avgRating = Number(total/data.reviews.length);
      data.totalRating = data.reviews.length;
      //console.log(data.avgRating);
      console.log(data);
      return data;
    }
  });

  app.addRating = Backbone.Model.extend({
    url: function(){
      if( this.get('menuId'))
        return '/tiffinBoxSupplierMenuRating/' + this.get('dabbawalaId')+'/'+this.get('menuId');
      else
        return '/tiffinBoxSupplierRating/' + this.get('dabbawalaId');
    }
  });

  app.TiffinBoxSupplier = Backbone.Model.extend({
    urlRoot: '/tiffinBoxSupplier'
  });

  app.addTiffinBoxSupplierMenu = Backbone.Model.extend({
    url: function() {
      if(this.get('dabbawalaId')) {
        if(this.get('menuId')){
          return '/tiffinBoxSupplierMenu/' + this.get('dabbawalaId')+'/'+ this.get('menuId');
        }
        else{
          return  '/tiffinBoxSupplierMenu/?dabbawalaId=' + this.get('dabbawalaId');  
        } 
      } else {
        return '/tiffinBoxSupplierMenu';  
      };
    }    
  });

  app.  = Backbone.Model.extend({
    url: function() {
      console.log('In Model'+this.get('dabbawalaId'));
      return '/calendar/menuDate/' + this.get('dabbawalaId');
    }    
  });

  app.changePassword = Backbone.Model.extend({
    urlRoot: '/changePassword'    
  });

  app.UserLoggedIn = Backbone.Model.extend({
    urlRoot: '/users/loggedIn'    
  });

  app.CartCollection = Backbone.Model.extend({
    url:function() {
      if(this.get('id')){
        if(this.get('singlecartid')){
          return '/cart/addtocart/' + this.get('id')+'/'+this.get('singlecartid');
        }
        else
      return '/cart/addtocart/' + this.get('id');
      }
      else{
        return '/cart/addtocart/';
      }
    }  
  });

  app.Order = Backbone.Model.extend({
    url:'/order'
  });

  app.UserLogin = Backbone.Model.extend({
    url: '/users/authenticate'
  });

  app.FacebookLogin = Backbone.Model.extend({
    url: '/users/fbauth'
  });

  app.UserForgotPassword = Backbone.Model.extend({
    url: '/users/forgotPassword'
  });

  app.UserResetPassword = Backbone.Model.extend({
    url: '/users/resetPassword'
  });

  // app.Logout = Backbone.Model.extend({
  //   url: '/users/logout'
  // });

  app.Logout = Backbone.Model.extend({
    url: '/admin/logout'
  });  
})();