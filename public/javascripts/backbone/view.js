

var app = app || {};

app.usr_flag= false;
app.fromSignUp= false;
app.loginNow = false;

(function () {
  'use strict';

  Backbone.View.prototype.close = function () {
    //COMPLETELY UNBIND THE VIEW
    this.undelegateEvents();
    this.$el.removeData().unbind();
    this.$el.empty();
  };

  app.LandingView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('landing-template').innerHTML
    ),
    events: {
      'click .consumerSearch' : 'storeQueryAndNavigate',
      'focus #comsumerQuery': 'getAutocomplete'
    },

    initialize: function() {
      app.View = this;
    },
    render: function () {
      var that = this;

      var navbarView= new app.NavbarView();
      navbarView.render();

      that.$el.html(that.tpl());
    },
    storeQueryAndNavigate:function(){
      var query=document.getElementById('#comsumerQuery').value;
      window.localStorage.setItem("searchItem", query);
      app.qr= $('#comsumerQuery').val();
      if($('#comsumerQuery').val()==""){
        $('.landingErrorMsg').show();
        return false;
      }
      app.router.navigate('list', {trigger: true});
    },
    getAutocomplete:function(ev){
      var searchArray = ['Veg','Nonveg','Lunch','Dinner','Breakfast','Daily','Monthly','Weekly','Hadapsar','Aundh','Dange chowk'];
     
      $('#comsumerQuery').autocomplete({        
        minLength: 1,
        source: searchArray,
        select: function (event, ui) {
          var query=$('#comsumerQuery').val();
          window.localStorage.setItem("searchItem", ui.item.value); 
          app.router.navigate('list', {trigger: true});
        }
        // error: function (event, ui) {
        // //error code here
        // }
      });
    }
  });
       
  app.CreateUserView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('create-user-template').innerHTML
    ),
    events: {
        'submit .create-user-form': 'saveUser'
    },
    initialize: function() {
    
    },
    render: function () {
      // var deleviryAddr= ["Home Address", ,];
      this.$el.html( this.tpl());
    },
    saveUser: function (ev) {
      var userDetails = $(ev.currentTarget).serializeObject();
      console.log(userDetails);
      userDetails.address = [];
      if(typeof(userDetails.vicinity)==='object'){
        for( var i =0 ; i < userDetails.vicinity.length; i++) {
          var address = {};
          address['vicinity'] = userDetails.vicinity[i];
          address['city'] = userDetails.city[i];
          address['state'] = userDetails.state[i];
          address['zipCode'] = userDetails.zipCode[i];
          address['title'] = userDetails.title[i];
          address['mealTypeDefoult'] = userDetails['addressMealtype_'+(i+1)];

          userDetails.address.push(address);
        }
      }else{
        var address = {};
          address['vicinity'] = userDetails.vicinity;
          address['city'] = userDetails.city;
          address['state'] = userDetails.state;
          address['zipCode'] = userDetails.zipCode;
          address['title'] = userDetails.title;
          address['mealTypeDefoult'] = userDetails['addressMealtype_1'];

          userDetails.address.push(address);
      }
      console.log('userDetails');
      console.log(userDetails);
      var user = new app.User();
      user.save(userDetails, {
        success: function(user){
          app.fromSignUp= true;
          app.router.navigate('signin',{trigger: true});
        },
        error: function (model, response) {
          //if(response.responseJSON.error.name === 'MongoError') {
            //document.getElementById('register-error-message').innerHTML = 'Email you entered is already registered with the application!';
          //}
        }
      });
      return false;
    }
  });

  app.SignInView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('login-user-template').innerHTML
    ),
    events: {
      'submit .login-user-form': 'login',
      'click #sign-in-with-fb': 'loginFb'
    },

    initialize: function() {
    
    },
    render: function () {
      var that = this;
      that.$el.html( that.tpl());
    },
    login: function(ev) {
      var loginDetails = $(ev.currentTarget).serializeObject();
      var userLogin = new app.UserLogin();

      userLogin.save(loginDetails, {
        success: function(user){ 
          if(user.attributes.role === 'admin') { 
            window.location = '/admin';
          } 
          else {

            var navbarView= new app.NavbarView();
            navbarView.render();

            if(app.fromSignUp){
              window.localStorage.setItem('fromAdmin','false');
              app.router.navigate('', {trigger: true});
            }else
            window.history.back();                     
          }
        },
        error: function (model, response) {
          if(_.isString(response.responseJSON.error)) {
            document.getElementById('error-message').innerHTML = response.responseJSON.error;
          }              
        }
      });
      return false;
    }
  });

  app.ForgotPasswordView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('forgotPassword-template').innerHTML
    ),
    events: {
      'submit .forgetPassword-user-form': 'saveUser'
    },
    initialize: function() {
    
    },
    render: function () {
      var that = this;
      that.$el.html( that.tpl());
    },
    saveUser: function (ev) {
      var forgetPasswordDetails = $(ev.currentTarget).serializeObject();
      var userForgotPassword = new app.UserForgotPassword();
      userForgotPassword.save(forgetPasswordDetails, {
        success: function(){
          app.router.navigate('signin',{trigger: true});
        },
        error: function(model, response){
          if(_.isString(response.responseJSON.error)) {
            document.getElementById('forgot-password-error').innerHTML = response.responseJSON.error;
          }      
        }
      });
      return false;
    }
  });    


  app.HomepageView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('homepage-template').innerHTML
    ),
    events: {
         
    },
    initialize: function() {
    
    },
    render: function () {
      var that = this;
      that.$el.html(that.tpl());
    }
       
  });


  app.ProfileView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('profile-template').innerHTML
    ),
    events: {
         
    },

    initialize: function() {
    
    },
    render: function () {
      var that = this;
      that.$el.html( that.tpl());
    }     
  });

  app.ListSupplierView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('list-template').innerHTML
    ),
    events: {
      'click .newSearch' :  'search',
      'submit .filer-search-form' : 'filterSearch',
      'click .viewMenu' : 'displayMenuList'
    },
    initialize: function() {
      app.View=this;
      app.fromSignUp= false;
    },
    render: function () {
      var that = this;
      var query=localStorage.getItem("searchItem");

      var navbarView= new app.NavbarView();
      navbarView.render();

      that.tiffinboxSupplier = new app.searchTiffinboxSupplier({query: query});
      that.tiffinboxSupplier.fetch({
        success: function(ev){
          that.$el.html( that.tpl({tiffinSupplers:that.tiffinboxSupplier.toJSON()}));
        }     
      });
    },
    search:function(){
      var query=document.getElementById('newComsumerQuery').value;
      localStorage.setItem("searchItem", query);
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      new app.ListSupplierView();
      this.render();
    },
    filterSearch : function(ev){
      var that = this
      app.View = this;
      var query = $(ev.currentTarget).serializeObject();

      var search = localStorage.getItem("searchItem");
      var searchFilterResult = new app.SearchFilterResult(JSON.stringify(query),search);
      
      searchFilterResult.fetch({
        success: function(ev){
          var content = '';
          for (var i = 0; i < searchFilterResult.models.length; i++){
            content += '<div class="col-md-4"> <div class="abc img-thumbnail"> <div><img src="../images/a1.jpg" class="img-responsive listOfSupplier_img"></div> <div class="center-text"> <h4>'+searchFilterResult.models[i].get('name')+'</h4>'+searchFilterResult.models[i].get('address').vicinity+'<br>'+searchFilterResult.models[i].get('address').city+'-'+searchFilterResult.models[i].get('address').zipCode+'.'+'<br>'+searchFilterResult.models[i].get('address').state+'. </div> <div class="lcb"> <input type="checkbox" class="listOfSuppliers" name="listOfSuppliers" value="'+searchFilterResult.models[i].get('_id')+'"> </div> </div> </div>';
          }
          //content +='html';
          $('.after_filter').html(content);
          $('.landingSearch').hide();
          $('.after_filter').show();
        },
        error: function(model, response){
          console.log('error:'+response);
        }
      });  
      return false;
    },
    displayMenuList: function(ev){
      var query=[];
      $(".listOfSuppliers:checked").each(function() {
          query.push(this.value);
      });
      window.localStorage.setItem('selectedTs',query);
      app.router.navigate('checkout', {trigger: true});
    }
  });   

  app.CheckoutView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('checkout-template').innerHTML
    ),
    events: {
      'click .addTocart': 'addTocart',
      'click .prevWeek': 'prevWeek',
      'click .nextWeek': 'nextWeek'
    },
    initialize: function() {
      app.View=this;

      if (app.View){
        app.View.close();
      }

      app.fromSignUp= false;
      app.firstDate= new Date();
    },
    render: function () { 
      var that = this;
      
      var navbarView= new app.NavbarView();
      navbarView.render();

      var selectedTs = window.localStorage.getItem('selectedTs');
      var tiffinboxSuppliers = new app.GetMenuDate({menuDate: selectedTs});
      tiffinboxSuppliers.fetch({
        success: function(ev){
          that.$el.html( that.tpl({tiffinSupplers:tiffinboxSuppliers.toJSON(), firstDate:app.firstDate}));
        }         
      });
    },
    addTocart:function(ev){
      var that = this;

      var cart = new app.CartCollection();      
      var orderDetails = [];

      $('.ckMenuId:checked').each(function(){
        var menuId=this.value;
        var dt=($(this).data('date')).split('/');
        var date = new Date(dt[2],dt[1],dt[0]);
        var mealType= $(this).data('mealtype');
        var ts_id= $(this).data('tiffinsupplier');
        var price= $(this).data('price');

        orderDetails.push({tiffinboxSupplier: ts_id,menuId:menuId, date:date, mealAt:mealType,price:price});
      });

      cart.save({orderDetails: orderDetails}, {
        success: function(cart){
          window.localStorage.setItem('cartId',cart.toJSON()._id);
          window.localStorage.setItem('cart',cart.toJSON());

          app.router.navigate('orderProcess',{trigger:true});
        },
        error: function(){
          console.log('error');
        }
      });

      return false;
    },
    prevWeek : function(){        
      console.log('in prevWeek button');
      this.counter--;
      if(this.counter==0){
        $('.prevWeek').attr("disabled", "disabled");           
      }
      app.firstDate.setDate(app.firstDate.getDate()-7);
      this.render();
    },
    nextWeek : function(){
      console.log('in nextWeek button'+ this.counter++);
      $('.prevWeek').removeAttr("disabled");
      app.firstDate.setDate(app.firstDate.getDate()+7);
      this.render();
    }                           
  });

  app.OrderProcessView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('order-detail-template').innerHTML
    ),
    events: {
      'click .btn-singlecartid': 'delteFromCart', 
      'click .continueOrder': 'continueOrder',
      'submit .login-user-form': 'login',
      'submit .create-user-form': 'createUser'
    },

    initialize: function() {
      app.View=this;
      app.fromSignUp= false; 
    },
    render: function () {
      var that = this;
      
      var navbarView= new app.NavbarView();
      navbarView.render();

      var cartId= window.localStorage.getItem('cartId');
      var cart= new app.CartCollection({id:cartId});
      cart.fetch({
        success:function(ev){
          var data = cart.toJSON().orderDetails;
          app.total= 0;
          for (var i=0; i<data.length; i++){
            app.total= app.total + data[i].price;
          }          
          app.item= i;

          that.$el.html(that.tpl({cartDetail: cart.toJSON(), total:app.total, item:app.item}));
        },
        error: function(){
          console.log('in error');
        }
      });        
      
    },
    delteFromCart: function(ev){
      var that = this;
      var singlecartid= $(ev.currentTarget).data('singlecartid');
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      
      var cartId= window.localStorage.getItem('cartId');
      var cart = new app.CartCollection({id:cartId, singlecartid:singlecartid});
      cart.save({},{
        success: function(cart){
          window.localStorage.setItem('cartId',cart.toJSON()._id);
          new app.OrderProcessView();
          that.render();        
        },
        error: function(model, response){
          console.log('in delteFromCart.error');
          console.log(response);
        }
      });   
      return false;   
    },
    continueOrder:function(ev){
      //console.log('continueOrder function called!');
      if(app.usr_flag){
        app.router.navigate('placeOrder',{trigger:true});
      }else{
        $('#modal-placeorder').modal('show');
      }       
    },
    login: function(ev){
      var loginDetails = $(ev.currentTarget).serializeObject();
      var loginUser = new app.UserLogin();
      window.localStorage.setItem('user_id', null);

      loginUser.save(loginDetails, {
        success: function(user){
          $('#modal-placeorder').hide();
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();

          app.router.navigate('placeOrder',{trigger: true}); 
        },
        error: function (model, response) {
          alert('login failded');
          if(_.isString(response.responseJSON.error)) {
            document.getElementById('error-message').innerHTML = response.responseJSON.error;
          }              
        }
      });
      return false;
    },
    createUser: function(ev){
      //console.log('at createUser function');
      var userInfo = $(ev.currentTarget).serializeObject();
      var user = new app.User();
      user.save(userInfo, {
        success: function(user){
          var login_User = new app.UserLogin();
          window.localStorage.setItem('user_id', null);

          login_User.save({email:user.attributes.email,password:user.attributes.password}, {
            success: function(user){
              $('#modal-placeorder').hide();
              $('body').removeClass('modal-open');
              $('.modal-backdrop').remove();
              app.usr_flag= true;
              app.uid= user.attributes._id;
              app.userEmail= user.attributes.email;

              var navbarView= new app.NavbarView();
              navbarView.render();

              app.router.navigate('placeOrder',{trigger:true});
            },
            error: function (model, response) {
              console.log('login failded:'+response);
              if(_.isString(response.responseJSON.error)) {
                document.getElementById('error-message').innerHTML = response.responseJSON.error;
              }              
            }
          });
        },
        error: function (model, response) {
          console.log('error:'+response);
          //if(response.responseJSON.error.name === 'MongoError') {
            //document.getElementById('register-error-message').innerHTML = 'Email you entered is already registered with the application!';
          //}
        }
      });
      return false;
    },      
  });

  app.PlaceOrderView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('place-order-page-template').innerHTML
    ),
    events: {          
      'submit .order-detail':'placeOrder1'
    },

    initialize: function() {
      app.View = this;
      
      if (app.View)
        app.View.close();
    },
    render: function () {
      //console.log('in placeOrderView render:');
      var that = this;

      var navbarView= new app.NavbarView();
      navbarView.render();

      var cartId= window.localStorage.getItem('cartId');
      var cart= new app.CartCollection({id:cartId});
      cart.fetch({
        success:function(ev){
          var data = cart.toJSON().orderDetails;
          app.total= 0;
          for (var i=0; i<data.length; i++){
            app.total= app.total + data[i].price;
          }
          app.item= i;
          that.$el.html(that.tpl({cartDetail: cart.toJSON(), total:app.total, item:app.item,userAddress:app.address, contact:app.contact, user:app.user}));
        },
        error: function(model, response){
          console.log('in error'+response);
        }
      });
    },
    placeOrder1:function(ev){
      //console.log('in placeOrder function');
      var cartId= window.localStorage.getItem('cartId');
      var cart = new app.CartCollection({id:cartId});
      var updateDetails = [];
      var dt = $(ev.currentTarget).serializeObject();
      var i=0;
      $(ev.currentTarget).find('.menuDate-d').each(function() {
        updateDetails.push({deliveryAddress:$(this).val(), dayId: $(this).attr('id')});
      });

      cart.save({updateDetails: updateDetails, contact:((app.usr_flag) ? app.user.contactNumber: undefined),userId:((app.usr_flag)? app.user._id: undefined)},{
        success:function(cart){
          $('#modal-conform-order').modal('show');

          $('#confirm').click(function(ev){
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            var order = new app.Order();
            order.save({order:cart}, {
              success: function(order){
                alert('order is confirmed');
                app.router.navigate('/',{trigger:true});
              }
              // error: function(model, response){
              //   console.log('in order.save.error');
              // }
            });
           });
        },
        error: function(model, response){
          console.log('in cart.save.error:'+response);
        }
      });

      return false;
    }         
  });

  app.getOrderView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('user-order-template').innerHTML
    ),
    events: {
    },

    initialize: function() { 
      app.fromSignUp= false;
    },
    render: function (options){
      var that = this;

      var navbarView= new app.NavbarView();
      navbarView.render();

      var getOrder = new app.orderCollections({userId: app.uid});
      getOrder.fetch({
        success: function(){;
          that.$el.html(that.tpl({orderData: getOrder.toJSON()}));
        },
        error: function(model, response){
          console.log('in get order error!');
        }

      });      
    }       
  });

  app.userAccountView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('profile-template').innerHTML
    ),
    events: {
      'submit .edit-user-basic-form': 'editBasicDetail',
      'submit .edit-user-password-form': 'editPasswordDetail'             
    },
    initialize: function() {
      app.fromSignUp= false;
    },
    render: function (options){
      var that = this; 
      var user= new app.UserLoggedIn();
      user.fetch({
        success:function(){
          if (user.toJSON().message){
            var user1 = new app.User({id: options.id});             
            user1.fetch({
              success:function(){

                var navbar= new app.NavbarView();
                navbar.render();

                localStorage.setItem('hash',user1.toJSON().hash);

                that.$el.html(that.tpl({user:user1.toJSON()}));
              },
              error:function(){
                alert('error');
              }
            });       
          }
          else{
            app.router.navigate('/',{trigger:true});
          }
        }
      });    
           
    },
    editBasicDetail: function(ev){
      //console.log('in editBasicDetail');
      var userInfo = $(ev.currentTarget).serializeObject();
      console.log(userInfo);
      userInfo.address = [];
      if(typeof(userInfo.vicinity)=='object'){
        for( var i =0 ; i < userInfo.vicinity.length; i++) {
          // var mealtype = userInfo['addressMealtype_'+i];
          // console.log(mealtype);
          var address = {};
          address['vicinity'] = userInfo.vicinity[i];
          address['city'] = userInfo.city[i];
          address['state'] = userInfo.state[i];
          address['zipCode'] = userInfo.zipCode[i];
          address['title'] = userInfo.title[i];
          address['mealTypeDefoult'] = userInfo['addressMealtype_'+i];
          console.log(address);

          userInfo.address.push(address);
        }
      }else{
        var address = {};
          address['vicinity'] = userInfo.vicinity;
          address['city'] = userInfo.city;
          address['state'] = userInfo.state;
          address['zipCode'] = userInfo.zipCode;
          address['title'] = userInfo.title;
          // address['mealTypeDefoult'] = userInfo.addressMealtype+'-'+0;
          address['mealTypeDefoult'] = userInfo['addressMealtype_0'];
          console.log(address);

          userInfo.address.push(address);
      }

      var userId= window.localStorage.getItem('user_id');
      var user = new app.User({id: userId});
      user.save(userInfo,{
        success:function(err, user){
          $('.userProfileUpdateSuccess').show();
        },
        error: function(model, response){
          console.log('in error:'+response);
        }

      });
      return false;
    },
    editPasswordDetail:function(ev){
      //console.log('editPasswordDetail');  
      var pswdDetail = $(ev.currentTarget).serializeObject();
      var change_pswd= new app.changePassword({id:app.uid});
      change_pswd.save(pswdDetail, {
        success: function(user){
          $('.editPasswordDetailSuccess').show();
          window.location.reload();
        },
        error:function(model,response){
          alert('error:'+response);
        }
      });
      return false;
    }         
  });   

  app.ShowTiffinSupplerView =Backbone.View.extend({
    el:'.page',
    tpl: Handlebars.compile(
      document.getElementById('show-tiffinsupplier-template').innerHTML
    ),
    events: {
      'click .rating': 'rating',
      'submit .login-user-form': 'login',
      'submit .create-user-form': 'createUser',
      'click .write-reviews': 'writeReview',
      'click .write-reviews-btn': 'writeReview',
      //'submit .write-reviews-form': 'saveReview'
      'click .submitReview': 'saveReview',
      'click .viewMenuDetail': 'viewMenuDetail',

      'click .ratingMenu': 'ratingMenu',
      'click .write-reviews-menu': 'writeReviewMenu',
      'click .write-reviews-menu-btn': 'writeReviewMenu',
      'click .submitReviewMenu': 'saveReviewMenu'
    },
    
    initialize: function() {
      app.View=this;
      app.fromSignUp= false;
      if(app.View)
        app.View.close();
    },
    render: function (options) {
      var that = this;

      var navbarView= new app.NavbarView();
      navbarView.render();

      that.tiffinboxSupplier = new app.addTiffinBoxSupplier({id: options.id});
      that.tiffinboxSupplier.fetch({
        success: function(user){
          window.localStorage.setItem('dabbawalaId',that.tiffinboxSupplier.toJSON()._id);

          that.$el.html( that.tpl({tiffinSuppler:that.tiffinboxSupplier.toJSON()}));
        },
        error: function(model, response){
          console.log('error:');
          console.log(response);
        }        

      });
    },
    rating: function(ev){ 
      if(!app.usr_flag){
        $('#modal-reviews').modal('show');
      }

      var that= this;
      app.myvar = setInterval(function () { that.myTimer(); }, 1000);      
    },
    myTimer: function(ev){
      if(app.usr_flag){
        clearInterval(app.myvar);

        var users = $('.rating').attr("data-orderedUser");
        users = users.split(',');
        console.log('users:'+users);
        var flag =false;
        for(var i = 0; i<users.length; i++){
          if(users[i]==app.user._id){
            flag = true;
            break;
          }
        }
        if(!flag){
          console.log('You should to orders before Reviews and Rating!');
          $('.userShouldHaveOrderForRating').text("You should to orders before Reviews and Rating!");
          $('.userShouldHaveOrderForRatingDiv').show();
          return false;
        }
        
        console.log('continuew to reviews or rating');

        var ratingDetails = {};
        ratingDetails.user = app.user._id;
        if(app.saveReview){
          //alert('Reviews and Rating !');
          ratingDetails.rate = $('.rating1').attr("data-value");
          ratingDetails.title = $('#reviwTitle').val();
          ratingDetails.review = $('#reviwComment').val();
        }else{
          //alert('Rating !');
          ratingDetails.rate = $('.rating').attr("data-value");        
        }
        
        var rating = new app.addRating({dabbawalaId: window.localStorage.getItem('dabbawalaId')});
        
        console.log('before saving data');
        console.log(ratingDetails);
        rating.save(ratingDetails,{
          success: function(rating){  
            console.log('in success');
            console.log(rating);
            window.location.reload();
          },
          error: function(model, response){
            console.log('error:');
            console.log(response);
          }
        });
      }
    },
    login: function(ev){
      var loginDetails = $(ev.currentTarget).serializeObject();
      var loginUser = new app.UserLogin();
      window.localStorage.setItem('user_id', null);

      loginUser.save(loginDetails, {
        success: function(user){
          $('#modal-reviews').hide();
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();

          var navbarView= new app.NavbarView();
          navbarView.render();

          //location.reload();
        },
        error: function (model, response) {
          alert('login failded');
          if(_.isString(response.responseJSON.error)) {
            document.getElementById('error-message').innerHTML = response.responseJSON.error;
          }              
        }
      });
      return false;
    },
    createUser: function(ev){
      //console.log('at createUser function');
      var userInfo = $(ev.currentTarget).serializeObject();
      var user = new app.User();
      user.save(userInfo, {
        success: function(user){
          var login_User = new app.UserLogin();
          window.localStorage.setItem('user_id', null);

          login_User.save({email:user.attributes.email,password:user.attributes.password}, {
            success: function(user){
              $('#modal-placeorder').hide();
              $('body').removeClass('modal-open');
              $('.modal-backdrop').remove();

              var navbarView= new app.NavbarView();
              navbarView.render();
            },
            error: function (model, response) {
              console.log('login failded:'+response);
              if(_.isString(response.responseJSON.error)) {
                document.getElementById('error-message').innerHTML = response.responseJSON.error;
              }              
            }
          });
        },
        error: function (model, response) {
          console.log('error:'+response);
          //if(response.responseJSON.error.name === 'MongoError') {
            //document.getElementById('register-error-message').innerHTML = 'Email you entered is already registered with the application!';
          //}
        }
      });
      return false;
    },         
    writeReview: function(ev){
      //alert('writeReview called');
      $('.write-reviews-btn').hide();
      $('.reviewDiv').show();

      if ($(".rating1").hasClass('exist')) {
        //alert('already initialized');
      }else {
          //alert('it is not initialized yet');
        $(".rating1").jRating({
          onClick : function(element,rate) {
            $('.rating1').attr("data-value",""+rate);
          },
          rateMax: 10,
          showRateInfo: false
        }); 
      }      
    },
    saveReview: function(ev){
      //alert('saveReview called');
      app.saveReview=true;
      this.rating();
    },
    viewMenuDetail: function(ev){
      console.log("menuId:"+$(ev.currentTarget).data('id'));
      window.localStorage.setItem('menuId', $(ev.currentTarget).data('id'));
      $('#modal-menuDetail').modal('show');
    },
    ratingMenu: function(ev){ 
      if(!app.usr_flag){
        $('#modal-reviews').modal('show');
      }
      var that= this;

      this.users = ($(ev.currentTarget).data('users')); 
      app.myvarMenu = setInterval(function () {that.myTimerMenu(); }, 1000);      
    },
    myTimerMenu: function(ev){
      if(app.usr_flag){
        clearInterval(app.myvarMenu);
        console.log(this.users);
        var users = this.users;
        users = users.split(',');
        console.log('users:'+users);
        var flag =false;
        for(var i = 0; i<users.length; i++){
          if(users[i]==app.user._id){
            flag = true;
            break;
          }
        }
        if(!flag){
          console.log('You should to orders before Reviews and Rating!');
          $('.userShouldHaveOrderForRating-menu-'+window.localStorage.getItem('menuId')).text("You should to orders before Reviews and Rating!");
          $('.userShouldHaveOrderForRatingDiv-menu-'+window.localStorage.getItem('menuId')).show();
          return false;
        }  
        console.log('continue to rating');
        var ratingDetails = {};
        ratingDetails.user = app.user._id;

        if(app.saveReviewMenu){
          //alert('Reviews and Rating !');
          ratingDetails.rate = $('.rating1Menu').attr("data-value");
          // ratingDetails.title = this.reviewDetails.title;
          // ratingDetails.review = this.reviewDetails.comment;
          ratingDetails.title = $('#reviwTitleMenu-'+window.localStorage.getItem('menuId')).val();
          ratingDetails.review = $('#reviwCommentMenu-'+window.localStorage.getItem('menuId')).val();
        }else{
          //alert('Rating !');
          ratingDetails.rate = $('.ratingMenu').attr("data-value");        
        }
        console.log(window.localStorage.getItem('menuId'));
        var rating = new app.addRating({dabbawalaId: window.localStorage.getItem('dabbawalaId')
          , menuId:window.localStorage.getItem('menuId')});
        
        console.log('before saving data');
        console.log(ratingDetails);
        rating.save(ratingDetails,{
          success: function(rating){  
            console.log('in success');
            console.log(rating);
            $('#modal-menuDetail-'+window.localStorage.getItem('menuId')).hide();
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            window.location.reload();
          },
          error: function(model, response){
            console.log('error:'+response);
          }
        });
      }
    },
    writeReviewMenu: function(ev){
      //alert('writeReview called');
      $('.write-reviews-menu-btn').hide();
      $('.reviewDivMenu').show();

      if ($(".rating1Menu").hasClass('exist')) {
        //alert('already initialized');
      }else {
          //alert('it is not initialized yet');
        $(".rating1Menu").jRating({
          onClick : function(element,rate) {
            //alert('rate from writeReview().......:'+rate);
            $('.rating1Menu').attr("data-value",""+rate);
          },
          rateMax: 10,
          showRateInfo: false
        }); 
      }      
    },
    saveReviewMenu: function(ev){
      alert('saveReview called');
      window.localStorage.setItem('menuId', $(ev.currentTarget).data('value'));
      app.saveReviewMenu=true;
      //this.reviewDetails= $(ev.currentTarget).serializeObject();
      this.ratingMenu();
    }

  });              

  app.FullNavbarView = Backbone.View.extend({
    el:'.navigation',
    tpl: Handlebars.compile(
      document.getElementById('full-navbar-template').innerHTML
    ),
    events: {
         
    },

    initialize: function() {
    
    },
    render: function () {
      var that = this;
      var cart = $.cookie('cart');
      if(typeof cart!='undefined'){
        cart = cart.substr(2);
        that.$el.html(that.tpl({cart: JSON.parse(cart)}));
      }
      else
        that.$el.html(that.tpl());
    }    
  });

  app.NavbarView =Backbone.View.extend({
    el:'.navigation',
    tpl: Handlebars.compile(
      document.getElementById('navbar-template').innerHTML
    ),
    events: {
      'click .logout': 'logoutUser',
      'click #btncart': 'btncart'
    },
    initialize: function() {
    
    },
    render: function () { 
      var that= this;
      
      var isUserLoggedIn = new app.UserLoggedIn();
      isUserLoggedIn.fetch({
        success:function(ev){
          console.log('success:'+isUserLoggedIn.toJSON().message);
          if(isUserLoggedIn.toJSON().message){
            app.user = isUserLoggedIn.toJSON().user;
            app.usr_flag= true;
            app.address= isUserLoggedIn.toJSON().user.address;
            
            var cart = $.cookie('cart');
            if(typeof cart != 'undefined'){
              cart = cart.substr(2);
              that.$el.html( that.tpl({userName: app.user.email, uid:app.user._id, cart: JSON.parse(cart)}));
            }
            else{
              that.$el.html( that.tpl({userName: app.user.email, uid:app.user._id}));   
            }            
          }
          else{
            var fullnavbarView= new app.FullNavbarView();
            fullnavbarView.render();
          }
        },
        error: function(){
          alert('error');
        }       
      });  
    },
    logoutUser: function() {
      var userLogout = new app.Logout();
      userLogout.fetch({
        success: function() {
          window.location.reload();
          //app.router.navigate('signin', {trigger: true});
        },
        error: function(model, response) {
          console.log('logout error:'+response);
        },
        btncart: function(){
          //alert('btncart');
        }      
      });
      return false;
    }     
  });
})();