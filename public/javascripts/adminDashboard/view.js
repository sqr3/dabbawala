var app = app || {};

(function () {
  'use strict'; //

  Backbone.View.prototype.close = function () {
    //COMPLETELY UNBIND THE VIEW
    this.undelegateEvents();
    this.$el.removeData().unbind();
    this.$el.empty();
  };

  app.AdminDashboardView =Backbone.View.extend({
    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('admin-dashboard-template').innerHTML
    ),

    events: {
      'click .search': 'searchResult'
    },

    initialize: function() {
      app.View = this;
      var userName = window.localStorage.getItem('userName');
      this.dabbawala = new app.addTiffinBoxSupplier();
      this.listenTo( this.dabbawala, 'sync', this.render,this);
      this.dabbawala.fetch();        
    },

    render: function () {
      var that = this;
      var userName= window.localStorage.getItem('userName');
      that.$el.html( that.tpl({dabbawalaList: that.dabbawala.toJSON(),userName: userName}));
                           
            
    },

    searchResult: function(ev){
      var query = $('#searchKey').val();
      this.tiffinboxSupplier = new app.searchTiffinboxSupplier({query: query});
      var that= this;
      that.tiffinboxSupplier.fetch({
        success: function(ev){
          var content= "";
          for (var i=0;i<that.tiffinboxSupplier.models.length;i++){
            content+='<tr><td class="active">'+that.tiffinboxSupplier.models[i].get('name')+'</td><td class="active">'+that.tiffinboxSupplier.models[i].get('address').vicinity+','+that.tiffinboxSupplier.models[i].get('address').city+','+that.tiffinboxSupplier.models[i].get('address').zipCode+'</td><td class="active">'+that.tiffinboxSupplier.models[i].get('distributionAreas')+'</td><td class="active"><a href="#edit/'+that.tiffinboxSupplier.models[i].get('_id')+'" class="btn btn-primary" id="">Edit</a><a href="#menuDate/'+that.tiffinboxSupplier.models[i].get('_id')+'" class="btn btn-primary" id="">MenuDate</a><a href="#delete/'+that.tiffinboxSupplier.models[i].get('_id')+'" class="btn btn-danger" id="">Delete</a></td></tr>';
          }
          $('.afterSearhTable').html(content);
          $('.startTable').hide();
          $('.newTable').show();
        }
      });         
      return false;      
    }        
  });
  
  app.addDabbawalaView =Backbone.View.extend({
    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('add-dabbawala-template').innerHTML
    ),

    events: {
      'submit .create-dabbawala-form':'saveDabbawala',
      'focus .input-tags': 'getAutocomplete'           
    },

    initialize: function() {
      app.View = this;
    },

    render: function () {
      var that = this;
      that.$el.html( that.tpl());           
    },
    saveDabbawala:function(ev){
      var dabbawalaDetails = $(ev.currentTarget).serializeObject();

      var dabbawala = new app.addTiffinBoxSupplier();

      dabbawala.save(dabbawalaDetails, {
        success: function(dbw){
          app.router.navigate('/',{trigger: true});
        },
        error: function(model, response){
          if(_.isString(response.responseJSON.error)) {            
            //document.getElementById('forgot-password-error').innerHTML = response.responseJSON.error;
          }      
        }
      });
      return false;
    },
    getAutocomplete:function(ev){
      var that = this;
       
      var searchArray = ['Veg','Nonveg','Lunch','Dinner','Breakfast','Daily','Monthly','Weekly','Hadapsar','Aundh','Dange chowk'];
      
      $('.input-tags').autocomplete({        
        minLength: 1,
        source: searchArray,
        select: function (event, ui) {
          var query=$('#input-distributionArea').val();
        }
        // error: function (event, ui) {
        // //error code here
        // }
      });
    }         
  });

  app.AdminReportView =Backbone.View.extend({
    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('admin-right-navbar-report-template').innerHTML
    ),
    events: {
      'change .suppliers-list':'getSuppliersOrders'
    },

    initialize: function() {
      this.current_date = new Date();
      this.dabbawala = new app.TiffinBoxSupplier();
      this.orders = new app.OrderDetails();
      this.listenTo( this.dabbawala, 'sync', this.render,this);
      var a = this.orders.fetch();
      this.dabbawala.fetch();
      console.log('inside view ');
      console.log(a);
    },
    render: function () {
      var that = this;
      $('#suppliers-order-table').hide();
      console.log(this.dabbawala.toJSON());
      this.$el.html( this.tpl({dabbawalaList: this.dabbawala.toJSON(),orderList:this.orders.toJSON(),firstDate:that.current_date}));
    },
    getSuppliersOrders:function(){
      var that = this;
      var dabbawalaId = $('.suppliers-list').val();
      this.orders = new app.OrderDetails({id:dabbawalaId});
      this.orders.fetch({
        success:function(ev){
          this.render();
          $('.tiffinboxSupplier-search').show();  
        }.bind(this)
      });
      console.log('suppliers orderDetails');
      console.log(this.orders.toJSON());

    }
         
  });

  app.FullNavbarView =Backbone.View.extend({
    el:'.navigation',

    tpl: Handlebars.compile(
      document.getElementById('full-navbar-template').innerHTML
    ),

    events: {
      'click .logout': 'logoutUser'
    },

    initialize: function() {
        
    },
    render: function () {
      var that = this;
      that.$el.html( that.tpl());
    },
    logoutUser: function() {
      var userLogout = new app.Logout();
        userLogout.fetch({
        success: function() {
          window.localStorage.setItem('fromAdmin','true');
          window.location = '/users/user#signin';
        },
        error: function(model, response) {
     
        }
      });
      return false;
    }         
  });

  app.AddTeamView =Backbone.View.extend({
    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('add-team-template').innerHTML
    ),

    events: {
      'submit .create-team-form':'saveTeam'
    },

    initialize: function() {
      app.View= this;

      this.dabbawala = new app.addTiffinBoxSupplier();
      this.listenTo( this.dabbawala, 'sync', this.render,this);
      this.dabbawala.fetch();
    },

    render: function () {
      var that = this;
      var dabbawalaName= window.localStorage.getItem('dabbawalaName');
      that.$el.html( that.tpl({dabbawalaList: that.dabbawala.toJSON(),dabbawalaName:dabbawalaName}));
    },

    saveTeam:function(ev){
      //console.log('in saveTeam');     

      var teamDetails = $(ev.currentTarget).serializeObject();
      var dabbawalaId = window.localStorage.getItem('dabbawalaId');

      var team = new app.User({dabbawalaId: dabbawalaId});
     
      team.save(teamDetails, {
        success: function(){
          window.localStorage.setItem('tiffinboxSupplierId', team.toJSON().tiffinboxSupplier);

          window.history.back();
          //app.router.navigate('teamList',{trigger: true});
        },
        error: function(model, response){
          console.log('in saveteam error');
          // if(_.isString(response.responseJSON.error)) {
            
          //   //document.getElementById('forgot-password-error').innerHTML = response.responseJSON.error;
          // }      
        }
      });
      return false;
    } 
         
  });

  app.EditTeamView =Backbone.View.extend({
    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('add-team-template').innerHTML
    ),

    events: {
      'submit .edit-team-form':'editTeam'
    },

    render: function (options) {
      var that=this;
      var userId = options.id;
      window.localStorage.setItem('userId',options.id);
      var dabbawalaId = window.localStorage.getItem('dabbawalaId');

      that.tiffinboxSupplier = new app.addTiffinBoxSupplier({id:dabbawalaId});
      that.tiffinboxSupplier.fetch({
        success: function (tiffinboxSupplier) { 
           window.localStorage.setItem('dabbawalaName', tiffinboxSupplier.attributes.name);
           window.localStorage.setItem('dabbawalaId',tiffinboxSupplier.attributes.id);
          for (var i = 0; i < tiffinboxSupplier.toJSON().team.length; i++) {
            if(tiffinboxSupplier.toJSON().team[i]._id === userId){
              var team = tiffinboxSupplier.toJSON().team[i];
              //console.log('record matched');
              break;
            }
          };
          that.$el.html(that.tpl({team: team, dabbawalaName: window.localStorage.getItem('dabbawalaName')}));
        }
      });
      return false;
    },

    editTeam:function(ev){
      //console.log('in editTeam');
      var teamDetails = $(ev.currentTarget).serializeObject();
      var userId = window.localStorage.getItem('userId');
      var team = new app.User({userId: userId});
     
      team.save(teamDetails,{
        success: function(){
          window.localStorage.setItem('tiffinboxSupplierId', team.toJSON().tiffinboxSupplier);
                  
          window.history.back();
        },
        error: function(model, response){
          console.log('in saveteam error');
          // if(_.isString(response.responseJSON.error)) {
            
          //   //document.getElementById('forgot-password-error').innerHTML = response.responseJSON.error;
          // }      
        }
      });
      return false;
    } 
         
  });

  app.DeleteTeamView= Backbone.View.extend({
    render: function (options) {
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();

      var that = this;
      var userId = options.id;
      if(userId){
        that.user = new app.User({id: userId});
        that.user.destroy({
          success: function (user){
            window.history.back();
            //app.router.navigate('edit', {trigger:true});
          }
        });
        return false;
      }
    }    
  }); 

  app.AddMenuView =Backbone.View.extend({
    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('add-menu-template').innerHTML
    ),

    events: {
      'submit .create-menu-form':'saveMenu',
      'click .btnback': 'backFun'  
    },

    initialize: function(){
      app.View= this;

      if(app.View){
        app.View.close();
      }

      this.dabbawala = new app.addTiffinBoxSupplier();
      this.listenTo( this.dabbawala, 'sync', this.render,this);
      this.dabbawala.fetch();

    },

    render: function () {  

      var that = this;
      var dabbawalaName= window.localStorage.getItem('dabbawalaName')
      ,mealType= window.localStorage.getItem('mealtype')
      ,category= window.localStorage.getItem('category');   

      that.$el.html( that.tpl({dabbawalaList: that.dabbawala.toJSON()
        ,dabbawalaName: dabbawalaName
        ,category:category.split(',')
        ,mealType:mealType.split(',')}));

    },
    backFun:function(ev){
      window.history.back();
    },

    saveMenu:function(ev){
      var menuDetails = $(ev.currentTarget).serializeObject();
      var dabbawalaId = window.localStorage.getItem('dabbawalaId');

      var menu = new app.addTiffinBoxSupplierMenu({dabbawalaId : dabbawalaId});
      menu.save(menuDetails, {
        success: function(){          
          window.localStorage.setItem('tiffinboxSupplierId', menu.toJSON()._id);
          window.location.reload();
        },
        error: function(model, response){
          alert('in savemenu error');
        }
      });
      return false;
    } 
  });

  app.EditMenuView =Backbone.View.extend({
    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('add-menu-template').innerHTML
    ),

    events: {
      'submit .edit-menu-form':'editMenu'  
    },

    render: function (options) {
      var that=this;
      var menuId = options.id;
      window.localStorage.setItem('menuId',menuId);
     
      var dabbawalaId = window.localStorage.getItem('dabbawalaId');

      that.tiffinboxSupplier = new app.addTiffinBoxSupplier({id:dabbawalaId});
      that.tiffinboxSupplier.fetch({
        success: function (tiffinboxSupplier) { 
          for (var i = 0; i < tiffinboxSupplier.toJSON().menu.length; i++) {
            if(tiffinboxSupplier.toJSON().menu[i]._id === menuId){
              var menu = tiffinboxSupplier.toJSON().menu[i];
              //console.log('record matched');
              break;
            }
          };
          var dabbawalaName= window.localStorage.getItem('dabbawalaName')
            ,mealType= window.localStorage.getItem('mealtype')
            ,category= window.localStorage.getItem('category');
   
          that.$el.html(that.tpl({menu: menu
            ,dabbawalaName: dabbawalaName
            ,category:category.split(',')
            ,mealType:mealType.split(',')}));
        }
      });
    },

    editMenu:function(ev){
      //console.log('in editMenu');
      var menuDetails1 = $(ev.currentTarget).serializeObject();
      var dabbawalaId = window.localStorage.getItem('dabbawalaId');
      var menu = new app.addTiffinBoxSupplierMenu({dabbawalaId : dabbawalaId, menuId : window.localStorage.getItem('menuId')});
      menu.save(menuDetails1, {
        success: function(){
          window.localStorage.setItem('tiffinboxSupplierId', menu.toJSON()._id);

          window.location.reload();
          //app.router.navigate('menuList',{trigger: true});
        },
        error: function(model, response){
          console.log('in editmenu error');
        }
      });
      return false;
    } 
  });

  app.DeleteMenuView= Backbone.View.extend({
    render: function (options) {
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      var that = this;
      var menuId = options.id;
      var dabbawalaId = window.localStorage.getItem('dabbawalaId');
      if(menuId){
        var menu = new app.addTiffinBoxSupplierMenu({dabbawalaId : dabbawalaId, menuId : menuId});
        menu.save({},{
          success: function (menu){
            window.history.back();
            //app.router.navigate('edit', {trigger:true});
          }
        });
        return false;
      }
    }        
  }); 

  app.AdminNavbarView =Backbone.View.extend({
    el:'.admin-navbar',
    tpl: Handlebars.compile(
      document.getElementById('admin-navbar-template').innerHTML
    ),
    events: {
             
    },

    initialize: function() {
        
    },
    render: function () {
      var that = this;
      that.$el.html( that.tpl());
    }
         
  });

  

  app.SearchListView =Backbone.View.extend({
    el:'.admin-content',
    tpl: Handlebars.compile(
      document.getElementById('search-dabbawala-template').innerHTML
    ),
    events: {
             
    },

    initialize: function() {
      app.View= this;

      var query = $('#searchKey').val();
      this.tiffinboxSupplier = new app.searchTiffinboxSupplier({query: query});
      this.listenTo( this.tiffinboxSupplier, 'sync', this.render,this);
      this.tiffinboxSupplier.fetch();        
    },

    render: function () {
      var that = this;
      that.$el.html( that.tpl({dabbawalaList: this.tiffinboxSupplier.toJSON()}));
    }
         
  });

  app.AdminRightNavbarDabbawalaView =Backbone.View.extend({
    el:'.admin-right-navbar',
    tpl: Handlebars.compile(
      document.getElementById('admin-right-navbar-dabbawala-template').innerHTML
    ),
    events: {
             
    },

    initialize: function() {
        
    },
    render: function () {
      var that = this;
      that.$el.html( that.tpl());
    }
         
  });

  app.AdminRightNavbarReportView =Backbone.View.extend({
    el:'.admin-right-navbar',

    tpl: Handlebars.compile(
      document.getElementById('admin-right-navbar-report-template').innerHTML
    ),
    events: {
             
    },

    initialize: function() {
        
    },
    render: function () {
      var that = this;
      that.$el.html( that.tpl());
    }
         
  });

  app.DeleteDabbawalaView= Backbone.View.extend({
    el: '.admin-content',
    render: function (options) {
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      var that = this;
      if(options.id){
        var id = options.id;
        that.tiffinboxSupplier = new app.addTiffinBoxSupplier({id:id});
        that.tiffinboxSupplier.destroy({
          success: function (tiffinboxSupplier){
            app.router.navigate('/', {trigger:true});
          }
        });
        return false;
      } 
    }    
  });

  app.EditDabbawalaView= Backbone.View.extend({
    el: '.admin-content',
    tpl: Handlebars.compile(
      document.getElementById('edit-dabbawala-template').innerHTML
    ),
    events: {
      'submit .edit-dabbawala-form':'update'    
    },
    render: function (options) {
      var that=this;
      if(options.id){
        var id=options.id;

        that.tiffinboxSupplier = new app.addTiffinBoxSupplier({id:id});
        that.tiffinboxSupplier.fetch({
          success: function (tiffinboxSupplier) { 
            window.localStorage.setItem('dabbawalaName', tiffinboxSupplier.attributes.name);
            window.localStorage.setItem('dabbawalaId',tiffinboxSupplier.attributes.id);
            window.localStorage.setItem('category',tiffinboxSupplier.attributes.category);
            window.localStorage.setItem('mealtype',tiffinboxSupplier.attributes.mealType);
            that.$el.html(that.tpl({tiffinboxSupplier:that.tiffinboxSupplier.toJSON()}));
          }
        })
      }
    },
    update: function(ev){
      //console.log('In update');
      var userDetails = $(ev.currentTarget).serializeObject();
      var tiffinboxSupplier= new app.TiffinBoxSupplier();
      tiffinboxSupplier.save(userDetails,{
        success: function(tiffinboxSupplier){
          app.router.navigate('adminDashboard', {trigger:true});
        },
        error: function(model, response){
          console.log('in error:'+response);
        }
      });
    }
  });

  app.MenuDateView =Backbone.View.extend({

    el:'.admin-content',

    tpl: Handlebars.compile(
      document.getElementById('menu-date-template').innerHTML
    ),

    events: {
      'submit .assign-menuDate-form' : 'menuDate'
    },

    initialize: function() {
      app.View= this;
    },

    render: function (options) {
      var that=this;
      if(options.id){
        var id=options.id;
        window.localStorage.setItem('id',id);

        that.tiffinboxSupplier = new app.addTiffinBoxSupplier({id:id});
        that.tiffinboxSupplier.fetch({
          success: function (tiffinboxSupplier) {  
            window.localStorage.setItem('dabbawalaName', tiffinboxSupplier.attributes.name);
            window.localStorage.setItem('dabbawalaId',tiffinboxSupplier.attributes.id);
            //console.log(tiffinboxSupplier);   
            that.$el.html(that.tpl({tiffinboxSupplier:that.tiffinboxSupplier.toJSON()}));
          }
        });
      }
      return false;
    },

    menuDate: function(ev){
      //console.log('in menuDate function');
      var menuDetails = $(ev.currentTarget).serializeObject();
      menuDetails.date= new Date(menuDetails.date).toISOString();
      var dabbawalaId = window.localStorage.getItem('dabbawalaId');
      var date = new app.AssignMenuDate({dabbawalaId : dabbawalaId});
      date.save(menuDetails, {
        success: function(){        
         window.location.reload();
        },
        error: function(model, response){
          console.log('in save menuDate error:'+response);
        }
      });

      return false;
    }         
  });
})();