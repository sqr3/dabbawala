Handlebars.registerHelper('isSelected', function(context, value) {
  if(context == value){;
    return true;
  }    
  else
    return false;
});

Handlebars.registerHelper('isPresent', function(array, value) {
  for(var i = 0 ; i < array.length; i++) {
  	if(array[i] == value) {
    //  console.log('matched');
  		return true;
  	}
  };
  return false;
});

Handlebars.registerHelper("foreach",function(arr,options) {
  if(options.inverse && !arr.length)
    return options.inverse(this);

  return arr.map(function(item,index) {
    item.$index = index;
    item.$first = index === 0;
    item.$last  = index === arr.length-1;
    return options.fn(item);
  }).join('');
});

Handlebars.registerHelper("countIndex",function(arr,options) {
  if(options.inverse && !arr.length)
    return options.inverse(this);
    //console.log('in countIndex:'+arr.length-1);

  return arr.length;

    
});

Handlebars.registerHelper("findMenuDate",function(arrDays,arrMenu,ts,mealType,v,options) {
  var today = new Date(app.firstDate);
  today.setDate(today.getDate()+v);
  var today1 = today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear();
  
  for(var i = 0; i< arrDays.length;i++){
    var d = new Date(arrDays[i].date);
    d = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear();
    if(d== today1){
      for(var j = 0; j < arrMenu.length; j++){
        var day = arrDays[i];        
        if(day[mealType].menuId==arrMenu[j]._id){         
          return options.fn({menu:arrMenu[j],date:today1,ts:ts});
        }
      }  
    }
  }
});

Handlebars.registerHelper('dateFormate', function(date) {
  var d = new Date(date);
  d = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear();
  return d;
});

Handlebars.registerHelper('menuDet', function(arr, id, options) {  
  if(arr){
    for(var i=0;i<arr.length;i++){
      if(id===arr[i]._id)
        return options.fn(arr[i]);
    }
  }
  else 
    return arr;  
});

Handlebars.registerHelper('thisweek',function(date,list){
  var arr = [];
  console.log('helper function thisweek');
  console.log(list);
  var date = new Date();
  for (var i = 0; i < 7; i++) {

       arr[i] =  '<th>' + date.getDate() +  '</th>';
       date.setDate(date.getDate() + 1);
    }; 
    return arr;
});

Handlebars.registerHelper('datefmt',function(date){
  var date = new Date(date);
  console.log(date.getMonth());
  return date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear();
});

Handlebars.registerHelper('menuName',function(menuId,dabawalalist){
  console.log('menuId');
  console.log(menuId);
  return menuId;
});