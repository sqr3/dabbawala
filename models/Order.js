
/**
 * Order  model
 */

var bcrypt = require('bcrypt-nodejs')
  , Tiffinboxsupplier = require('./TiffinboxSuppliers')
  , User = require('./User')
  , mongoose = require('mongoose');


var orderSchema = mongoose.Schema({
    userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },

  orderDetails:[{
    tiffinboxSupplier: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Tiffinboxsupplier'
    },

    menuId: {
       type: mongoose.Schema.Types.ObjectId,
       ref: 'Tiffinboxsupplier.menu'
    },

    date: Date,
    mealAt: {
      type: String,
      enum: ['breakfast', 'lunch', 'dinner', 'snacks']
    },
    price: Number, //Daily will come from menu.price, monthly and weekly will come from dabbawala.price  

    deliveryAddress:{
      type: String,
      required: true
    }

    // there sholde be field for order type ie. Daily/Weekly/Monthly
    
  }],

  contactNumber: {
    type: Number,
    required: true
  },

  orderDate:{
    type: Date,
    default: new Date(),
  },
  orderTotal: Number,
  discount: Number,
  coupon: String
});


module.exports = mongoose.model('Order', orderSchema);
